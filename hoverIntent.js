'use strict';

function HoverIntent(options) {

  var timerId;
  this.elem = options.elem;
  
  function handlerEnter(e) {
  	timerId = setTimeout(options.over, 500);
  }

  function handlerOut() {
  	if (timerId) clearTimeout(timerId);
  	options.out();
  }
  
  this.elem.addEventListener('mouseenter', handlerEnter);
  this.elem.addEventListener('mouseleave', handlerOut);


  this.destroy = function() {
    delete this;
  };

}