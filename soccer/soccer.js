document.addEventListener('mousedown', handlerDown);

var heroes = document.body.getElementsByClassName('draggable');
var shiftX, shiftY, target;
var docEl = document.documentElement;
var maxZ = 999;



function handlerDown(e) {

	
	
	target = e.target;
	
	shiftX = e.clientX - target.getBoundingClientRect().left;
	shiftY = e.clientY - target.getBoundingClientRect().top; 

	var maxX = e.clientX - shiftX;


	if (target.className.indexOf('draggable') != -1) {
		
		document.addEventListener('mousemove', handlerMove);
		document.addEventListener('mouseup', handlerUp);
		target.style.zIndex = maxZ++;
		target.style.position = 'fixed';

		moveTo(target, e.clientX - shiftX + 'px', e.clientY - shiftY + 'px');
	}	
}


function handlerMove(e) {
	var topX = e.clientX - shiftX;
	if (topX<0) {
		topX=0;
	} else if (e.clientX - shiftX + target.offsetWidth > docEl.clientWidth) {
		console.log(e.clientX, docEl.offsetWidth, shiftX);
		topX = docEl.offsetWidth  - target.offsetWidth;
	}


	moveTo(target, topX + 'px', e.clientY - shiftY + 'px');
	var topMax = parseInt(target.style.top);
	var topMin = docEl.clientHeight - (parseInt(target.style.top) + target.offsetHeight);
	var topX = e.clientX - shiftX;
	
	if (topMax<=0) {
		moveTo(target, topX + 'px', e.clientY - shiftY - topMax  + 'px');
		docEl.scrollBy(0,-10);
	

	} else if ( topMin < 0) {
		moveTo(target, topX + 'px', e.clientY - shiftY + topMin + 'px');
		docEl.scrollBy(0,10);
		var scrollHeight = Math.max(
		  document.body.scrollHeight, document.documentElement.scrollHeight,
		  document.body.offsetHeight, document.documentElement.offsetHeight,
		  document.body.clientHeight, document.documentElement.clientHeight
		);
		if (e.clientY - (shiftY - target.offsetHeight) <= Math.ceil(docEl.clientHeight)) {
			shiftY -= 10;
			topMin += 10;
		}	

	}
	// console.log(e.clientY - (shiftY + docEl.scrollTop - target.offsetHeight), docEl.clientHeight);
	e.preventDefault()
}

function handlerUp(e) {
	e.target.style.top = parseInt(e.target.style.top) + pageYOffset + 'px'
	e.target.style.position = 'absolute';
	document.removeEventListener('mousemove', handlerMove);
	document.removeEventListener('mouseup', handlerUp);
}


function moveTo(e, left, top) {
	e.style.left = left;
	e.style.top = top;
}
