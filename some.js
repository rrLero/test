if (document.documentElement.matches === undefined) {
	Object.defineProperty(Element.prototype, 'matches', { // (2)
    get: function() {
      return Element.prototype.msMatchesSelector || Element.prototype.matchesSelector || Element.prototype.webkitMatchesSelector
    }
  });

}